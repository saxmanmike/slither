# SLither
[![pipeline status](https://gitlab.com/saxmanmike/slither/badges/master/pipeline.svg)](https://gitlab.com/saxmanmike/slither/commits/master)
[![coverage report](https://gitlab.com/saxmanmike/slither/badges/master/coverage.svg)](https://gitlab.com/saxmanmike/slither/commits/master)
Welcome to SLither, the ScienceLogic EM7/SL1 API web tool!

# Configuration
Environment configuration is managed via environmental variables.

## Required Variables:
`BASE`: The base URL for your ScienceLogic SL1 instance, i.e. "https://sl1.yourcompany.com"

`USER`: The name of a user that has SL1 API access.

`PASS`: The password for the above user. **Note: PLEASE do not pass this in plaintext!**
Use of a password manager with a CLI tool (1Password) is recommended for development purposes, and an enterprise password manager (HashiCorp Vault, AWS SSM, CyberArk, etc) is recommended for production environments. The example below uses the 1Password CLI with jq for easy parsing.

`REDIS_HOST`: The URL of your Redis host. Redis is used to cache organization names and speed up certain reports. If you are using the Docker image provided, a local Redis server is available. Otherwise you must run redis-server locally or connect to an external instance.

## Optional Variables:
`INSECURE_SSL`: Defaults to False. Set value to `True` if you want to make GET and POST requests without verifying SSL certificates. Certain SL1 development environments will require this to function. DO NOT SET THIS IN PRODUCTION!

# How to run
Linux:
```shell
BASE="https://sl1.yourcompany.com" \
USER="em7admin" \
PASS=$(op get item em7admin_password | jq '.details.fields[] | select(.designation=="password").value')\
REDIS_HOST="localhost" \
python3 ./slither.py
```

Windows:
```powershell
$env:BASE="https://sl1.yourcompany.com";`
$env:USER="em7admin";`
$env:PASS=$(op get item em7admin_password | jq '.details.fields[] | select(.designation=="password").value');`
$env:REDIS_HOST="localhost";`
python ./slither.py
```

Docker:

First, build the container from the Dockerfile:
```shell
docker build -t sl1ther:latest .
```
Next, run the container with environmental variables set.

Windows:
```powershell
docker container run `
  --env BASE="https://sl1.yourcompany.com" `
  --env USER="em7admin" `
  --env PASS=$(op get item em7admin_password | jq '.details.fields[] | select(.designation=="password").value') ` 
  --env REDIS_HOST="localhost" `
   sl1ther:latest
```
Linux:
```powershell
docker container run \
  --env BASE="https://sl1.yourcompany.com" \
  --env USER="em7admin" \
  --env PASS=$(op get item em7admin_password | jq '.details.fields[] | select(.designation=="password").value') \
  --env REDIS_HOST="localhost" \
   sl1ther:latest
```

# Security
Please note that the app is currently written to run without a WSGI server. This is not intended for production environments and a server such as [Green Unicorn](https://gunicorn.org/) is recommended for a secure, production-ready deployment. SL1ther should only be used in its current form a secure development environment.