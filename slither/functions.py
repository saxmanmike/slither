"""Call functions for SL1ther."""

import json
import re

import requests


def get_org(base, USER, PASS, test=False, ssl=False):
    """Get a list of organization names and IDs. Used throughout the application.

    Returns:
    choices (dict): The dictionary of organization IDs and names.

    """
    choices = {}

    if test:
        data = [
            {
                "URI": "/api/organization/0",
                "description": "System"
            },
            {
                "URI": "/api/organization/1",
                "description": "Test1"
            },
        ]
    if not test:
        url = base + "/api/organization?limit=1000&hide_filterinfo=1"
        r = requests.get(url, auth=(USER, PASS), verify=ssl)
        data = r.json()

    # Parsing it all into a usable form
    for items in data:
        choices.update({items["URI"]: items["description"]})
    return choices


class Functions:
    """Call functions for SL1ther."""

    def __init__(self, logger, USER, PASS, base, cache, test=False, ssl=True):
        """Initialize variables from Flask.

        Args:
            logger: Flask logger
            USER (str): API username.
            PASS (str): API password.
            base (str): Base URL for SL1 stack.
            cache: redis.Redis instance for caching
            test (bool): Run using test data. Defaults to False.
            ssl (bool): Whether to execute request with SSL verification. Defaults to True.
        """
        self.logger = logger
        self.USER = USER
        self.PASS = PASS
        self.base = base
        self.cache = cache
        self.test = test
        self.ssl = ssl

    def set_sku(self, sku, dids):
        """Set a custom attribute called SKU for a list of DIDs.

        Arguments:
        sku (str): The SKU to set.
        dids (List[str]): A list of DIDs on which to set the SKU.

        Please note: there are no tests for this function because there is no public SL1 instance to run against.

        """
        body = json.dumps({"c-SKU": sku})
        status = []

        for did in dids:
            URI = f"{self.base}/device/{str(did)}"
            r = requests.post(URI, auth=(self.USER, self.PASS), data=body, verify=self.ssl)
            status.append(str(r.status_code))
        return status

    def devicegroup_report(self, URI):
        """
        Generate a report of all devices in a given device group.

        Arguments:
        URI (str): A string representation of the device group's number

        Returns a dictionary.
        """
        report = {}

        # Generating a list from the device group
        if self.test:
            data = ['/api/device/2']
            orgs = ""
        if not self.test:
            orgs = json.loads(self.cache.get("orgs"))
            try:
                url = f"{self.base}/device_group/{URI}/expanded_devices/"
                r = requests.get(url, auth=(self.USER, self.PASS), verify=self.ssl)
                data = r.json()
            except Exception as e:
                self.logger.error(f"Exception: {e}")
                return None
            if r.status_code >= 400:
                self.logger.error(f"Device group API query failed with status {data.status_code}")
                return None

        # Pull in detailed data for each device
        for dev in data:
            if self.test:
                scrape = {'name': 'Tester', 'ip': '8.8.8.8', 'organization': '/api/organization/0'}
            if not self.test:
                try:
                    URL = self.base[:-4] + dev
                    x = requests.get(URL, auth=(self.USER, self.PASS), verify=self.ssl)
                    if x.status_code >= 400:
                        self.logger.error(f"Device API query failed with status {data.status}")
                        return None
                    scrape = x.json()
                except Exception as e:
                    self.logger.error(f"Exception: {e}")
                    return None

            # Process data
            if scrape["organization"] in orgs:
                org = orgs[scrape["organization"]]
            else:
                org = scrape["organization"]
            DID = dev.replace("/api/device/", "")
            name = scrape["name"]
            IP = scrape["ip"]
            SKU = "N/A"
            if "c-SKU" in scrape:
                SKU = scrape["c-SKU"]

            # Populate the dictionary
            report[DID] = {
                "Name": name,
                "IP": IP,
                "SKU": SKU,
                "Company": org,
            }
        return report

    def empty_skus(self):
        """Generate a report of devices with IPs and with empty SKUs.

        Can be edited to use any custom field - just replace c-SKU with c-<your value>
        """
        report = {}
        url_filter = "filter.ip.not=&filter.c-SKU.isnull=1&filter.organization.not=/api/organization/0"
        if self.test:
            data = {
                '/api/device/2': {
                    'name': 'Tester',
                    'ip': '8.8.8.8',
                    'class_type': '/api/device_class/4063',
                    'organization': '/api/organization/0',
                    'c-SKU': '',
                },
                '/api/device/3': {
                    'name': 'ec2-18-210-50-20',
                    'ip': '18.210.50.20',
                    'class_type': '/api/device_class/4063',
                    'organization': '/api/organization/0',
                    'c-SKU': '',
                }
            }
            orgs = False
        if not self.test:
            orgs = json.loads(self.cache.get("orgs"))
            r = self.iterate_devices(self.base, self.USER, self.PASS, url_filter)
            data = r.json()

        for k in data:
            try:
                DID = k.replace("/api/device/", "")
                name = data.get(k).get("name")
                IP = data.get(k).get("ip")
                if orgs and data.get(k).get("organization") in orgs:
                    org = orgs[data.get(k).get("organization")]
                else:
                    org = data.get(k).get("organization")
                SKU = data.get(k).get("c-SKU")
                URI = data.get(k).get("class_type")

                # Drilling down for device class
                if self.test:
                    class_name = category = data.get(k).get("class_type")
                if not self.test:
                    x = requests.get(self.base[:-4] + URI, auth=(self.USER, self.PASS), verify=self.ssl)
                    info = x.json()
                    class_name = f"{info.get('class')} {info.get('description')}"
                    URI = info.get("device_category")

                    # Drilling down for device category
                    y = requests.get(self.base + URI, auth=(self.USER, self.PASS), verify=self.ssl)
                    final = y.json()
                    category = final.get("cat_name")
            except KeyError:
                message = "All customer devices with an IP address have a SKU. Thank you!"
                return {
                    "DID": message,
                    "Name": message,
                    "IP": message,
                    "SKU": message,
                    "Company": message,
                    "Class": message,
                    "Category": message,
                }

            report[DID] = {
                "DID": DID,
                "Name": name,
                "IP": IP,
                "SKU": SKU,
                "Company": org,
                "Class": class_name,
                "Category": category,
            }
        return report

    def merge_two_dicts(self, x, y):
        """Merge two dictionaries.

        In Python 3, there are better ways to do this, but in order to maintain compatibility
        I've included this function for merging dictionaries. See iterate_devices() for a use case.
        """
        z = x.copy()
        z.update(y)
        return z

    def iterate_devices(self, parameters=""):
        """Iterate through devices in the API in groupings of 250.

        The ScienceLogic API has roughly a 2 million character limit per GET request.
        In order to get all devices, it is best to iterate in groupings of 250.

        Arguments:
        parameters (str): Parameters to append to device endpoint. Defaults to "".

        Returns:
        total_responses (Optional[dict]): Response JSON object from API call.
        """
        data = requests.get(f"{self.base}/api/device?limit=1")
        total_devices = data.json()["total_matched"]

        if float(total_devices) % 250 == 0:
            iterations = float(total_devices) / 250
        iterations = int((total_devices) / 250) + 1

        i = 0
        total_responses = {}
        try:
            while True:
                response = requests.get(
                    f"{self.base}/api/device?limit=250&hide_filterinfo=1&extended_fetch=1&offset=\
                        {i * 250}&{parameters}",
                    auth=(self.USER, self.PASS),
                )
                total_responses = self.merge_two_dicts(response.json(), total_responses)
                i += 1
                if i > iterations:
                    break
            return total_responses
        except Exception as e:
            self.logger.error(f"Exception occurred in iterate_devices(): {e}")
            return None

    def convert_severity(self, severity):
        """Return the severity string for a given number.

        Attributes:
        sev_list (dict): A mapping of severity numbers and their associated strings.

        Returns:
        sev_list[severity] (str): The severity string associated with the passed number.
        """
        sev_list = {"0": "Healthy", "1": "Notice", "2": "Minor", "3": "Major", "4": "Critical"}
        return sev_list[severity]

    def get_event(self, event_id, active):
        """Get information on an SL1 active or inactive event, given its event ID.

        Arguments:
        event_id (str): The ID of the event in SL1
        active (str): Whether or not the event is active. If it is, value will be "on"

        Returns:
        report (dict): A dictionary of the relevant information for an event in SL1.

        """
        report = {}
        endpoint = "/cleared_event"
        if active == "on":
            endpoint = "/event/"
        if self.test:
            event_data = {"severity": "1", "event_policy": "/api/event_policy/3694"}
            data = {"name": "Trap: Received trap from unknown device once"}

        if not self.test:
            URI = self.base + endpoint + event_id + "?hide_filterinfo=1&extended_fetch=1"
            r = requests.get(URI, auth=(self.USER, self.PASS), verify=self.ssl)
            event_data = r.json()

            if r.status_code != 200:
                return {
                    "uri": event_id,
                    "number": event_id,
                    "name": "Not found",
                    "severity": "Not found",
                    "link": "#",
                    "details": "#",
                }

            p = requests.get(self.base + event_data["event_policy"], auth=(self.USER, self.PASS), verify=self.ssl)
            data = p.json()

        report["uri"] = event_data.get("event_policy")
        report["number"] = event_data["event_policy"].strip("/api/event_policy")
        report["severity"] = self.convert_severity(event_data.get("severity"))
        report["name"] = data["name"]
        url_filter = "/em7/index.em7?exec=registry&act=registry_events#events_search.id="
        report["link"] = self.base[:-4] + url_filter + report["number"]
        details = self.base[:-4] + "/em7/index.em7?exec=event_management_editor&idx=" + report["number"]
        report["details"] = details

        return report

    def add_notes(self, note, dids):
        """Add a note to one or more devices.

        Arguments:
        note (str): The note to add to your device(s).
        dids (List[str]): List of DIDs to which the note should be added.

        Please note: there are no tests for this function because there is no public SL1 instance to run against.
        """
        status = []
        body = json.dumps({"note_text": note})

        for did in dids:
            URI = f"{self.base}/device/{str(did)}/note/"
            r = requests.post(URI, auth=(self.USER, self.PASS), data=body, verify=self.ssl)
            status.append(str(r.status_code))
        return status

    def see_notes(self, dids):
        """Display all notes on a given series of devices.

        Arguments:
        dids (list): A list of device IDs for which to pull notes.

        Returns:
        status (List[dict]): A list of dictionaries with device endpoints and notes.

        """
        status = []

        if self.test:
            data = [{"URI": "/api/device/2/note/1", "description": "Test note"}]
        if not self.test:
            for did in dids:
                URI = f"{self.base}/device/{str(did)}/note?hide_filterinfo=1&limit=1000"
                r = requests.get(URI, auth=(self.USER, self.PASS), verify=self.ssl)
                data = r.json()
        for note in data:
            status.append(note["description"])
        return status

    def cidr(self, ips, subnet, octet):
        """Strip off the value of a given octet and return CIDR notation for the remaining IP.

        Arguments:
        ips (List[str]): IP addresses to iterate through.
        subnet (str): The subnet mask. Must be between 1-32.
        octet (str): How many octets to strip. Values are "one", "two", or "three".

        Returns:
        ip_addrs_new (List[str]): A list of IP addresses in CIDR notation with the new subnet mask.

        """
        if (int(subnet) < 1) or (int(subnet) > 32):
            return "Invalid subnet mask! Please type a number between 1 and 32."

        ip_addrs_new = []

        for ip in ips:
            if octet == "one":
                matchObj = re.search(r"(\d{1,3}\.\d{1,3}\.\d{1,3})\.", ip, re.M)
                ip_addrs_new.append(matchObj.group(1) + ".0/" + subnet)
            if octet == "two":
                matchObj = re.search(r"(\d{1,3}\.\d{1,3})\.", ip, re.M)
                ip_addrs_new.append(matchObj.group(1) + ".0.0/" + subnet)
            if octet == "three":
                matchObj = re.search(r"(\d{1,3})", ip, re.M)
                ip_addrs_new.append(matchObj.group(1) + ".0.0.0/" + subnet)
        return ip_addrs_new

    def ip_extractor(self, raw_text):
        """Given a block of text, find all IP addresses and return a list of them.

        Arguments:
        raw_text (str): The block of text to search.

        Returns:
        matches (List[str]): A list of IP addresses.

        """
        regex = r"\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3}"  # IPv4 format
        matches = re.findall(regex, raw_text)
        return matches

    def serial_report(self, org):
        """Generate a report of devices and their serial numbers, if available.

        Arguments:
        org (dict): A mapping of organization API routes and names.

        """

        def _process_data(data, orgs):
            """Generate a report of devices and their serial numbers.

            Arguments:
            data (dict): SL1 API data
            orgs (Union[Object, dict]): Redis cache, or dictionary if testing

            """
            report = {}
            for dev in data.values():
                if dev["serial"] != "":
                    report[str(dev["device"]).replace("/api/device/", "")] = {
                        "Name": dev["device"],
                        "DID": dev["device"].strip("/api/device/"),
                        "Organization": orgs[dev["organization"]],
                        "Make": dev["make"],
                        "Model": dev["model"],
                        "Serial": dev["serial"],
                    }
            return report

        if self.test:
            orgs = {"/api/organization/0": "System"}
            data = {
                "/api/asset/3": {
                    "device": "/api/device/3",
                    "organization": "/api/organization/0",
                    "make": "",
                    "model": "",
                    "serial": "TEST-VALID",
                }
            }
            report = _process_data(data, orgs)
            report["3"]["Name"] = "TestDevice"
            report["3"]["IP"] = "8.8.8.8"

        if not self.test:
            org_id = org.strip("/api/organization/")
            url_filter = (
                "/asset/?limit=10000&extended_fetch=1&hide_filterinfo=1&filter.serial.isnull=0&filter.organization=")
            orgs = json.loads(self.cache.get("orgs"))

            # Generating a list from the device group
            r = requests.get(self.base + url_filter + org_id, auth=(self.USER, self.PASS), verify=self.ssl)
            data = r.json()
            report = _process_data(data, orgs)

            try:  # Get a name for the device instead of just its URI
                for dev in report.values():
                    # Pull individual device information
                    url = self.base.strip("/api") + dev["Name"] + "/?extended_fetch=1&hidefilter_info=1"
                    q = requests.get(url, auth=(self.USER, self.PASS), verify=self.ssl)
                    device = q.json()

                    for k, v in report.items():
                        if v == dev:
                            location = k
                    report[location]["Name"] = device.get("name")
                    # Since we're here anyway...add in the IP, if it exists
                    report[location]["IP"] = device.get("ip")
            except Exception as e:
                self.logger.error(f"An error occurred: {e}")
        return report
