"""SL1ther, for ScienceLogic SL1 stacks."""

import json
import os

import redis
import requests
from flask import Flask, render_template, request
from slither.functions import Functions, get_org

app = Flask(__name__)
__version__ = "0.8.3"

if os.getenv("BASE"):
    app.config["BASE"] = str(os.getenv("BASE")) + "/api"
if os.getenv("USER"):
    app.config["USER"] = os.getenv("USER")
if os.getenv("PASS"):
    app.config["PASS"] = os.getenv("PASS")
if os.getenv("REDIS_HOST"):
    app.config["REDIS_HOST"] = os.getenv("REDIS_HOST")
app.config["INSECURE_SSL"] = False
if os.getenv("INSECURE_SSL") == "True":
    app.config["INSECURE_SSL"] = True

if app.config["BASE"] is None:
    app.logger.error("Base URL is required. Please re-run setting BASE=<your ScienceLogic URL>.")
    exit()
if app.config["USER"] is None or app.config["PASS"] is None:
    app.logger.error("Authentication information is required for most functions.\
            Please re-run and set USER and PASS environment variables.")
    exit()

try:
    r = app.config["r"] = redis.Redis(host=app.config["REDIS_HOST"])
    # TODO: Add possibility of Redis authentication
    orgs = get_org(app.config["USER"], app.config["PASS"], app.config["BASE"])
    r.set("orgs", json.dumps(orgs))
except Exception as e:  # TODO: Specific exception
    app.logger.error("No Redis connection available. Re-run setting REDIS_HOST appropriately.")
    app.logger.error(f"Exception: {e}")
    exit()

functions = Functions(app.logger, app.config["BASE"], app.config["PASS"], app.config["base"], r,
                      app.config["INSECURE_SSL"])


@app.route("/")
def index():
    """View the home page."""
    return render_template("index.html", version=__version__)


@app.route("/sku", methods=["GET", "POST"])
def assign_sku():
    """Assign a SKU to one or more devices."""
    if request.method == "POST":
        sku = request.form.get("sku")
        dids = request.form["dids"].split(",")
        try:
            status = functions.set_sku(sku, dids)
        except:
            app.status_code = 500
        data = {}
        for item, did in enumerate(dids):
            data[did] = {"sku": sku, "status": status[item], "username": functions.USER}
        app.logger.info(data)
        return render_template("sku_results.html", data=data)
    return render_template("sku.html")


@app.route("/devgroup_report", methods=["GET", "POST"])
def devgroup_report(USER=app.config["USER"], PASS=app.config["PASS"], base=app.config["BASE"]):
    """Generate a device group report."""
    if request.method == "POST":
        try:
            uri = request.form.get("uri")
            report = functions.devicegroup_report(uri)
            if report is None:
                app.status_code = 500
            return render_template("devgroup_results.html", report=report)
        except requests.exceptions.RequestException as e:
            return e

    r = requests.get(f"{base}/device_group?limit=1000", auth=(USER, PASS), verify=app.config["INSECURE_SSL"])
    data = []
    for dev in r.json()["result_set"]:
        data.append({dev["URI"].replace("/api/device_group/", ""): dev["description"]})

    return render_template("devgroup_report.html", data=data)


@app.route("/empty_skus", methods=["GET", "POST"])
def empty_sku_page():
    """Get a list of devices that don't have a custom attribute called SKU."""
    if request.method in ("GET", "POST"):
        report = functions.empty_skus()
        return render_template("empty_sku_results.html", report=report)


@app.route("/event_id", methods=["GET", "POST"])
def event_page():
    """Get information on an event based on its event ID."""
    if request.method == "POST":
        event_id = request.form.get("event_id")
        active = request.form.get("active")
        report = functions.get_event(event_id, active)
        return render_template("event_result.html", report=report)
    return render_template("event.html")


@app.route("/add_notes", methods=["GET", "POST"])
def add_notes_page():
    """Add a note to one or more devices by DID."""
    if request.method == "POST":
        note = request.form.get("note")
        dids = request.form.get("dids").split(",")
        try:
            status = functions.add_notes(note, dids)
        except:
            app.status_code = 500
        data = {}
        for item, did in enumerate(dids):
            data[did] = {"note": note, "status": status[item], "username": functions.USER}
            app.logger.info(data)
        return render_template("add_notes_results.html", data=data)
    return render_template("add_notes.html")


@app.route("/see_notes", methods=["GET", "POST"])
def see_notes_page():
    """See all notes on one or more devices."""
    if request.method == "POST":
        dids = request.form.get("dids").split(",")
        data = functions.see_notes(dids)
        report = {}
        for item, did in enumerate(dids):
            report[did] = {"note": data[item]}
        return render_template("see_notes_results.html", report=report)
    return render_template("see_notes.html")


@app.route("/cidr", methods=["GET", "POST"])
def view_cidr():
    """Rapidly strip 1-3 octets and add a subnet mask, returning CIDR notation."""
    if request.method == "POST":
        ips = request.form.get("ips").split("\r\n")
        subnet = request.form.get("subnet")
        octet = request.form.get("octet")

        stripped = functions.cidr(ips, subnet, octet)
        results = "\r\n".join(stripped)
        original = "\r\n".join(ips)
        return render_template("iptocidr_results.html", original=original, results=results)
    return render_template("iptocidr.html")


@app.route("/extractor", methods=["GET", "POST"])
def view_ip():
    """Pull IP addresses out of a large block of text."""
    if request.method == "POST":
        raw_text = request.form.get("raw_text")
        results = functions.ip_extractor(raw_text)
        return render_template("ipextract_results.html", results=results)
    return render_template("ipextract.html")


@app.route("/serial", methods=["POST", "GET"])
def serial_report():
    """Generate a list of devices and their serial numbers, if they have them."""
    if request.method == "POST":
        company = request.form.get("company")
        report = functions.serial_report(company)
        return render_template("serial_results.html", report=report)
    r = app.config["r"]
    data = json.loads(r.get("orgs"))
    return render_template("serial.html", data=data)


@app.errorhandler(500)
def server_error(e):
    """Handle HTTP 500 errors."""
    app.logger.error(f"An error occurred during a request: {e}")
    return (f"""
    An internal error ocurred: <pre>{e}</pre>
    See logs for full stacktrace.
    """, 500)


@app.errorhandler(404)
def not_found(e):
    """It's me, 404."""
    app.logger.error("Page not found.")
    return render_template("404.html"), 404


if __name__ == "__main__":
    app.run(port=5000, debug=False, threaded=True)
# [END app]
