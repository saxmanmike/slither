"""Test functions for SL1ther."""

import logging
from slither.functions import get_org, Functions

logging.basicConfig(format='%(asctime)s %(message)s')
functions = Functions(logging, None, None, "", None, True)


class TestIPExtractor:
    """Test IP Extractor function."""

    def test_ip_extractor(self):
        """Test for a positive."""
        text = open("tests/ip_text.txt").read()
        assert functions.ip_extractor(text) == ["10.0.0.1"]

    def test_negative(self):
        """Test for a negative."""
        text = open("tests/ip_text.txt").read()
        assert functions.ip_extractor(text[9:]) == []


class TestCIDR:
    """Test CIDR notation function."""

    def test1(self):
        """Test for positive /24."""
        assert functions.cidr(["10.0.0.1", "192.168.1.1", "8.8.8.8", "172.17.17.3"], "24",
                              "one") == ["10.0.0.0/24", "192.168.1.0/24", "8.8.8.0/24", "172.17.17.0/24"]

    def test2(self):
        """Test for negative, subnet mask is too large."""
        result = functions.cidr(["10.0.0.0"], "64", "one")
        assert result == "Invalid subnet mask! Please type a number between 1 and 32."

    def test3(self):
        """Test for negative, subnet mask is a negative number."""
        result = functions.cidr(["10.0.0.0"], "-3", "one")
        assert result == "Invalid subnet mask! Please type a number between 1 and 32."

    def test4(self):
        """Test for positive /16."""
        assert functions.cidr(["10.0.0.1", "192.168.1.1", "8.8.8.8", "172.17.17.3"], "16",
                              "two") == ["10.0.0.0/16", "192.168.0.0/16", "8.8.0.0/16", "172.17.0.0/16"]

    def test5(self):
        """Test for positive /8."""
        assert functions.cidr(["10.0.0.1", "192.168.1.1", "8.8.8.8", "172.17.17.3"], "8",
                              "three") == ["10.0.0.0/8", "192.0.0.0/8", "8.0.0.0/8", "172.0.0.0/8"]


class TestSerialReport:
    """Test for serial number report."""

    def test1(self):
        """Test for positive."""
        assert functions.serial_report("/api/organization/0") == {
            "3": {
                "Name": "TestDevice",
                "DID": "3",
                "Organization": "System",
                "Make": "",
                "Model": "",
                "Serial": "TEST-VALID",
                "IP": "8.8.8.8",
            }
        }


class TestOrg:
    """Test to get organization mapping."""

    def test1(self):
        """Test for positive."""
        assert get_org(None, None, None, test=True) == {
            "/api/organization/0": "System",
            "/api/organization/1": "Test1",
        }


class TestNotes:
    """Test for viewing notes on a device."""

    def test1(self):
        """Test for positive."""
        assert functions.see_notes(None) == ["Test note"]


class TestMerge:
    """Test for the ability to merge two dictionaries."""

    def test1(self):
        """Test for positive."""
        assert functions.merge_two_dicts({"foo": 1}, {"bar": 2}) == {"foo": 1, "bar": 2}


class TestEvent:
    """Test for viewing events on a device."""

    def test1(self):
        """Test for positive."""
        assert functions.get_event("", "") == {
            "uri": "/api/event_policy/3694",
            "number": "3694",
            "name": "Trap: Received trap from unknown device once",
            "severity": "Notice",
            "link": "/em7/index.em7?exec=registry&act=registry_events#events_search.id=3694",
            "details": "/em7/index.em7?exec=event_management_editor&idx=3694",
        }


class TestSKU:
    """Test for generating a list of devices with empty custom attribute SKU."""

    def test1(self):
        """Test for positive."""
        assert functions.empty_skus() == {
            "2": {
                "DID": "2",
                "Name": "Tester",
                "IP": "8.8.8.8",
                "SKU": "",
                "Company": "/api/organization/0",
                "Class": "/api/device_class/4063",
                "Category": "/api/device_class/4063",
            },
            "3": {
                "DID": "3",
                "Name": "ec2-18-210-50-20",
                "IP": "18.210.50.20",
                "SKU": "",
                "Company": "/api/organization/0",
                "Class": "/api/device_class/4063",
                "Category": "/api/device_class/4063",
            }
        }


class TestDeviceGroup:
    """Test for device group report."""

    def test1(self):
        """Test for positive."""
        assert functions.devicegroup_report("") == {
            "2": {
                "Name": "Tester",
                "IP": "8.8.8.8",
                "SKU": "N/A",
                "Company": "/api/organization/0",
            }
        }
