from setuptools import setup, find_packages

setup(name='sl1ther',
      version='0.8.3',
      description='ScienceLogic Python wrapper, extending the EM7/SL1 API.',
      url='https://gitlab.com/saxmanmike/slither',
      author='Michael Gray',
      author_email='mike@graywind.org',
      license='MIT',
      packages=find_packages(),
      zip_safe=False)
