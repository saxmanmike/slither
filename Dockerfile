FROM alpine:latest as builder
LABEL author="Mike Gray (mike@graywind.org)"

RUN apk add --update ca-certificates curl gcc libc-dev make tar linux-headers && \
  wget http://download.redis.io/redis-stable.tar.gz && \
  tar xvzf redis-stable.tar.gz && \
  cd redis-stable && \
  make


FROM docker.cloudsmith.io/mike-gray/slither/pandaflask:latest

COPY --from=builder /redis-stable/src/redis-server /usr/bin/redis-server
ADD . /slither
WORKDIR /slither
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["/bin/sh", "./run.sh"]
